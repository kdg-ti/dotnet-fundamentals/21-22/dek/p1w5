﻿using System;
using CA.Domain;
using CA.EF;
using Microsoft.EntityFrameworkCore;

namespace CA
{
    class Program
    {
        static void Main(string[] args)
        {
            MyDbContext ctx = new MyDbContext();
            ctx.Database.EnsureDeleted();
            var isDbCreatedNow = ctx.Database.EnsureCreated();
            
            // insert data
            Person p = new Person() { Name = "Kenneth", Age = 41 };

            ctx.People.Add(p);
            ctx.SaveChanges();
            
            
            Console.WriteLine("Hello World!");
        }
    }
}